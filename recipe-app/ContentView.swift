//
//  ContentView.swift
//  recipe-app
//
//  Created by Даниил Тимонин on 23.01.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
