//
//  recipe_appApp.swift
//  recipe-app
//
//  Created by Даниил Тимонин on 23.01.2023.
//

import SwiftUI

@main
struct recipe_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
