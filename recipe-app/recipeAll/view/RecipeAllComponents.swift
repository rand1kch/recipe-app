//
//  RecipeAllComponents.swift
//  recipe-app
//
//  Created by Даниил Тимонин on 23.01.2023.
//

import SwiftUI

struct RecipeAllTitle: View {
    let title: String
    
    var body: some View {
        HStack(alignment: .center) {
            VStack(alignment: .leading){
                Text(title)
                    .font(.system(size: 20).bold())
                Spacer().frame(height: 5)
                Text("What are you cooking today?").font(.system(size: 11)).foregroundColor(Color("Gray3"))
            }
            Spacer()
            RoundedRectangle(cornerRadius: 10).frame(width: 40,height: 40)
        }
    }
}

struct RecipeAllFilter: View {
    
    let filters: [String]
    @Binding var selectedFilter: String
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack{
                ForEach(filters, id: \.self) { filter in
                    Text(filter)
                        .font(.system(size: 11).bold())
                        .foregroundColor(filter == selectedFilter ? .white : Color("primary80"))
                        .padding(.horizontal, 20)
                        .padding(.vertical, 7)
                        .background(filter == selectedFilter ? Color("primary100") : .white)
                        .cornerRadius(10)
                        .onTapGesture{
                            selectedFilter = filter
                        }
                }
                Spacer().frame(width: 30)
            }.padding(.vertical, 10)
                .padding(.horizontal, 30)
        }
    }
}

struct SearchFilterTextField : View {
    
    @Binding var searchText: String
    let hint: String
    
    var body: some View {
        HStack{
            SearchTextField(searchText: $searchText, hint: hint)
            Spacer().frame(width: 30)
            Button(action: {}, label: {
                Image("ic-filter")
                    .frame(width: 20, height: 20)
                    .padding(10)
            }).background(Color("primary100"))
                .cornerRadius(10)
        }
    }
}

struct SearchTextField : View {
    
    @Binding var searchText: String
    let hint: String
    
    var body: some View {
        TextField(hint,text: $searchText)
            .padding(.vertical, 12)
            .padding(.horizontal, 38)
            .background(RoundedRectangle(cornerRadius: 10)
                .strokeBorder(lineWidth:1)
                .foregroundColor(Color("Gray4")))
            .foregroundColor(.black)
            .font(.system(size: 11))
            .textFieldStyle(.plain)
            .overlay(HStack {
                Image("ic-search")
                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                    .foregroundColor(Color("Gray4"))
                    .padding(.leading, 10)
            })
    }
}

struct RecipeAllComponents_Previews: PreviewProvider {
    static var previews: some View {
        RecipeAllTitle(title: "Hello Jega")
        SearchTextField(searchText: .constant(""), hint: "Search recipe")
        RecipeAllFilter(filters: ["1","2","3","4","5"], selectedFilter: .constant("2"))
    }
}
