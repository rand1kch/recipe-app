//
//  RecipeAllView.swift
//  recipe-app
//
//  Created by Даниил Тимонин on 23.01.2023.
//

import SwiftUI

struct RecipeAllView: View {
    
    @State private var searchText: String = ""
    @State private var selectedFilter: String = "All"
    
    private let filters = ["All","Indian","Italian","Asian","Chinese"]
    
    var body: some View {
        ScrollView {
            LazyVStack(alignment:.leading) {
                Spacer().frame(height: 20)
                RecipeAllTitle(title: "Hello Jega").padding(.horizontal, 30)
                
                Spacer().frame(height:30)
                SearchFilterTextField(searchText: $searchText, hint: "Search recipe")
                    .padding(.horizontal, 30)
                
                
                Spacer().frame(height: 15)
                RecipeAllFilter(filters: filters, selectedFilter: $selectedFilter)
                
                Spacer().frame(height: 15)
                ZStack {
                    VStack(alignment: .center) {
                        Spacer().frame(height: 66)
                        Text("Classic Greek\nSalad")
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("Gray1"))
                            .font(.system(size: 14).bold())
                        Spacer()
                        HStack(alignment: .bottom){
                            VStack{
                                Text("Time")
                                    .font(.system(size: 11))
                                    .foregroundColor(Color("Gray3"))
                                Spacer().frame(height: 5)
                                Text("15 mins")
                                    .font(.system(size: 11).bold())
                                    .foregroundColor(Color("Gray1"))
                            }
                            Spacer()
                            Image("ic-search")
                        }
                    }
                    .padding(10)
                    .frame(width: 150, height: 176)
                    .background(
                        RoundedRectangle(cornerRadius:12)
                            .foregroundColor(Color("Gray3"))
                            .opacity(0.5)
                    )
                    .alignmentGuide(VerticalAlignment.center){
                        $0[.top]
                    }
                    Circle().frame(width: 110, height: 110).foregroundColor(.yellow)
                }
            }
        }
    }
}

struct RecipeAllView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeAllView()
    }
}
