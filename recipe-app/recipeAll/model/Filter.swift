//
//  Filter.swift
//  recipe-app
//
//  Created by Даниил Тимонин on 23.01.2023.
//

import Foundation

struct Filter: Identifiable {
    let id = UUID()
    let filter: String
}
